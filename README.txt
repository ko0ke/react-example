
NodeJS sample with NodeJS+(ReactJS/handlebars)+MongoDB

npm install after unzip file.

This nodejs sample needs a mongodb server.
    - Script to initialize the database with tables is "initDB.js" with the command "mongo initDB.js"
    - Then write in a terminal "mongod" to launch the database.

To use ReactJS.
    - npm run watch-react

To use handlebars. With handlebars I only have one page.
    -  npm run watch-hb

To launch de nodejs server
    - node app.js