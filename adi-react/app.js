var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var path = require('path');
var port = process.env.PORT || 3000;

app.use('/web', express.static('web'))
app.use(bodyParser.json());
app.use(require('./controllers'));

// CONEXION CON BDD LOCAL
var db_connection = 'mongodb://localhost/adi-react';

mongoose.Promise = global.Promise;
mongoose.connect(db_connection, function(err) {
  if (err) {
    console.log('No es posible la conexion con Mongo.' + err);
  } else {
    app.listen(port, function() {
      console.log('Escuchando en el puerto: ' + port);
    })
  }
})

module.exports = app;