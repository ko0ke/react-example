var handlebars = require('handlebars');

// Plantilla para la lista de items
var templateLista = `
	<div class="container">
		<h2>Lista de Usuarios</h2>
		<table class="table table-condensed">
			<thead>
				<tr>
					<th>Login</th>
					<th>Email</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				{{#each this}}
					<tr>
						<td>{{login}}</td>
						<td>{{email}}</td>
						<td>
							<a id={{id}} href="javascript:detallesItem('{{id}}')" class="btn btn-info">Detalles</a>&nbsp;
							<a id={{id}} href="javascript:editarItem('{{id}}')" class="btn btn-warning">Editar</a>&nbsp;
							<a id={{id}} href="javascript:eliminarItem('{{id}}')" class="btn btn-danger">Eliminar</a>
						</td>
					</tr>
				{{/each}}
		  	</tbody>
		</table>
		<a href="javascript:nuevoItem()" class="btn btn-primary">Nuevo usuario</a>
	</div>
`

var templateListaVacia = `
	<div class="container">
		<h2>Lista de Usuarios</h2>
		<table class="table table-condensed">
			<thead>
				<tr>
					<th>Login</th>
					<th>Email</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="3">No existen usuarios en la base de datos</td>
				</tr>
		  	</tbody>
		</table>
		<a href="javascript:nuevoItem()" class="btn btn-primary">Nuevo usuario</a>
	</div>
`

var templateDetalles = `
	<div class="container">
		<h2 class="text-center">Detalles del producto con id: {{id}}</h2>
		<div class="row">
			<div class="col-sm-5"></div>
			<div class="col-sm-3">
				<label for="lblLogin">Login</label>
				<p>{{login}}</p>
				<label for="lblEmail">Email</label>
				<p>{{email}}</p>
				<p class="text-center">
					<a href="javascript:listado()" class="btn btn-primary">Volver</a>
				</p>
			</div>
			<div class="col-sm-5"></div>
		</div>
	</div>
`

var templateEditar = `
	<div class="container">
		<h2 class="text-center">Detalles del producto con id: {{id}}</h2>
		<div class="row">
			<div class="col-sm-5"></div>
			<div class="col-sm-3">
				<label for="lblLogin">Login</label>
				<input class="form-control" value={{login}} type="text" id="textLoginID" required/>
				<label for="lblEmail">Email</label>
				<input class="form-control" value={{email}} type="text" id="textEmailID" required/>
				<label for="lblPassword">Password</label>
				<input class="form-control" value={{password}} type="password" id="textPasswordID" required/>
				<br />
				<p class="text-center">
					<a href="javascript:guardar('{{id}}')" class="btn btn-primary">Modificar</a>
					<a href="javascript:listado()" class="btn btn-primary">Volver</a>
				</p>
			</div>
			<div class="col-sm-5"></div>
		</div>
	</div>
`

var templateNuevo = `
	<div class="container">
		<h2 class="text-center">Nuevo usuario</h2>
		<div class="row">
			<div class="col-sm-5"></div>
			<div class="col-sm-3">
				<label for="lblLogin">Login</label>
				<input class="form-control" type="text" id="textLoginID" required/>
				<label for="lblEmail">Email</label>
				<input class="form-control" type="text" id="textEmailID" required/>
				<label for="lblPassword">Password</label>
				<input class="form-control" type="password" id="textPasswordID" required/>
				<br />
				<p class="text-center">
					<a href="javascript:guardar()" class="btn btn-primary">Guardar</a>
					<a href="javascript:listado()" class="btn btn-primary">Volver</a>
				</p>
			</div>
			<div class="col-sm-5"></div>
		</div>
	</div>
`

// Compilamos los templates
var templateLista_compilada = handlebars.compile(templateLista)
var templateListaVacia_compilada = handlebars.compile(templateListaVacia)
var templateDetalles_compilada = handlebars.compile(templateDetalles)
var templateEditar_compilada = handlebars.compile(templateEditar)
var templateNuevo_compilada = handlebars.compile(templateNuevo)

document.addEventListener('DOMContentLoaded', function(){
	fetch('http://localhost:3000/users', {
		method: 'GET',
		headers: {
			'Content-type':'application/json'
		}
	}).then((response) => { 
		if (response.ok) return response.json()
	}).then((responseData) => {
		var listado
		if (typeof responseData._embedded !== 'undefined' && typeof responseData._embedded.users !== 'undefined') {
			if(responseData._embedded.users.length > 0) listado = templateLista_compilada(responseData._embedded.users)
			else listado = templateLista_compilada([responseData._embedded.users])
			document.getElementById("cuerpo").innerHTML = listado
		} else {
			listado = templateListaVacia_compilada()
			document.getElementById("cuerpo").innerHTML = listado
		}
	})
})

function listado() {
	fetch('http://localhost:3000/users', {
		method: 'GET',
		headers: {
			'Content-type':'application/json'
		}
	}).then((response) => { 
		if (response.ok) return response.json()
	}).then((responseData) => {
		var listado
		if (typeof responseData._embedded !== 'undefined' && typeof responseData._embedded.users !== 'undefined') {
			if(responseData._embedded.users.length > 0) listado = templateLista_compilada(responseData._embedded.users)
			else listado = templateLista_compilada([responseData._embedded.users])
			document.getElementById("cuerpo").innerHTML = listado
		} else {
			listado = templateListaVacia_compilada()
			document.getElementById("cuerpo").innerHTML = listado
		}
	})
}

function detallesItem(id) {
	fetch('http://localhost:3000/users/' + id, {
		method: 'GET',
		headers: {
			'Content-type':'application/json'
		}
	}).then((response) => { 
		if (response.ok) return response.json()
	}).then((responseData) => {
		var item = templateDetalles_compilada(responseData)
		document.getElementById("cuerpo").innerHTML = item
	})
}
function editarItem(id) {
	fetch('http://localhost:3000/users/' + id, {
		method: 'GET',
		headers: {
			'Content-type':'application/json'
		}
	}).then((response) => { 
		if (response.ok) return response.json()
	}).then((responseData) => {
		var item = templateEditar_compilada(responseData)
		document.getElementById("cuerpo").innerHTML = item
	})
}
function guardar(id) {
	var method = 'POST'
	var url = 'http://localhost:3000/users'
	if(id) { 
		method = 'PUT'
		url = 'http://localhost:3000/users/' + id
	}
	var nuevo = {}
   	nuevo.login = document.getElementById('textLoginID').value
	nuevo.email = document.getElementById('textEmailID').value
	nuevo.password = document.getElementById('textPasswordID').value
	fetch(url, {
		method: method,
		headers: {
			'Content-type':'application/json'
		},
		body: JSON.stringify(nuevo)
	}).then((response) => { 
		if (response.ok) return response.json()
	}).then((responseData) => {
		if (responseData) listado()
	})
}
function eliminarItem(id) {
	fetch('http://localhost:3000/users/' + id, {
		method: 'DELETE',
		headers: {
			'Content-type':'application/json'
		}
	}).then((response) => { 
		if (response.ok) listado()
	})
}
function nuevoItem() {
	var item = templateNuevo_compilada()
	document.getElementById("cuerpo").innerHTML = item
}

window.listado = listado
window.detallesItem = detallesItem
window.editarItem = editarItem
window.guardar = guardar
window.eliminarItem = eliminarItem
window.nuevoItem = nuevoItem