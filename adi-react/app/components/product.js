import React from 'react';
import Auth from './services/auth';

const Product = React.createClass({
	getInitialState : function() {
		return ({product: ''})
	},
	paginaProducto: function() { 
		if(this.props.product == 'bike') this.props.handleCambiarComponente('bikes', this.props.login, '')
		if(this.props.product == 'component') this.props.handleCambiarComponente('components', this.props.login, '')
		if(this.props.product == 'accesory') this.props.handleCambiarComponente('accesories', this.props.login, '')
	},
	componentWillMount : function() {
		var url
		if(this.props.product == 'bike') url = 'http://localhost:3000/bikes/'
		if(this.props.product == 'component') url = 'http://localhost:3000/components/'
		if(this.props.product == 'accesory') url = 'http://localhost:3000/accesories/'
		console.log('URL: ' + url+this.props.id)
		if(this.props.id) {
			fetch(url+this.props.id, {
				method: 'GET',
				headers: {
					'Content-type':'application/json'
				}
			}).then((response) => { 
				if (response.ok) return response.json()
			}).then((responseData) => {
				if (responseData) {
					this.setState({product: responseData})
				}
			})
		}
	},
	productSubmit: function(event) {
		var url, method, producto
		if(this.props.product == 'bike') url = 'http://localhost:3000/bikes/'
		if(this.props.product == 'component') url = 'http://localhost:3000/components/'
		if(this.props.product == 'accesory') url = 'http://localhost:3000/accesories/'
		if(this.props.estado == 'nuevo') method = 'POST'
		if(this.props.estado == 'detalles') method = 'GET'
		if(this.props.estado == 'modificar') method = 'PUT'
		if(this.props.estado != 'nuevo') url += this.props.id
		if(this.props.estado != 'detalles') {
			producto = {
				tipo: this.tipo.value,
				marca: this.marca.value,
				precio: this.precio.value,
				cantidad: this.cantidad.value
			}
		}
		fetch(url, {
			method: method,
			headers: {
				'Authorization': 'Bearer ' + Auth.getToken(),
				'Content-type':'application/json'
			},
			body: JSON.stringify(producto)
		}).then((response) => { 
			if (response.ok) return response.json()
		}).then((responseData) => {
			if (responseData) {
				this.paginaProducto();
			}
		})
		event.preventDefault()
	},
	handleChangesForm: function() {
		var producto = {
			tipo: this.tipo.value,
			marca: this.marca.value,
			precio: this.precio.value,
			cantidad: this.cantidad.value
		}
		this.setState({product: producto})
	},
	render: function() {
		console.log('Producto: ' + this.props.product + ' Estado: ' + this.props.estado)
		var titulo, opcionesTipo, opcionesMarca, inputPrecio, inputCantidad, boton, botonVolver
		if(this.props.product == 'bike') {
			if(this.props.estado == 'nuevo') {
				titulo = <h2 className="text-center">Nuevo producto</h2>
				opcionesTipo = <select ref={(campoTipo)=>{this.tipo=campoTipo}} className="form-control" id="selectTipo"><option>Carretera</option><option>Montaña</option></select>
				opcionesMarca = <select ref={(campoMarca)=>{this.marca=campoMarca}} className="form-control" id="selectMarca"><option>BH</option><option>Specialized</option><option>Trek</option><option>Cervelo</option></select>
				inputPrecio = <input ref={(campoPrecio)=>{this.precio=campoPrecio}} className="form-control" type="number" defaultValue="0" id="textPrecio" />
				inputCantidad = <input ref={(campoCantidad)=>{this.cantidad=campoCantidad}} className="form-control" type="number" defaultValue="0" id="textCantidad" />
				boton = <button onClick={this.productSubmit} className="btn btn-primary center-block" >Añadir</button>
				botonVolver = <button onClick={this.paginaProducto} className="btn btn-primary center-block" >Volver</button>
			}
			if(this.props.estado == 'detalles') {
				titulo = <h2 className="text-center">Detalles del producto con id: {this.props.id}</h2>
				opcionesTipo = <p>{this.state.product.tipo}</p>
				opcionesMarca = <p>{this.state.product.marca}</p>
				inputPrecio = <p>{this.state.product.precio}</p>
				inputCantidad = <p>{this.state.product.cantidad}</p>
				botonVolver = <button onClick={this.paginaProducto} className="btn btn-primary center-block" >Volver</button>
			} 
			if(this.props.estado == 'modificar') {
				titulo = <h2 className="text-center">Modificar el producto con id: {this.props.id}</h2>
				opcionesTipo = <select ref={(campoTipo)=>{this.tipo=campoTipo}} className="form-control" id="selectTipo" onChange={this.handleChangesForm} value={this.state.product.tipo}><option>Carretera</option><option>Montaña</option></select>
				opcionesMarca = <select ref={(campoMarca)=>{this.marca=campoMarca}} className="form-control" id="selectMarca" onChange={this.handleChangesForm} value={this.state.product.marca}><option>BH</option><option>Specialized</option><option>Trek</option><option>Cervelo</option></select>
				inputPrecio = <input ref={(campoPrecio)=>{this.precio=campoPrecio}} className="form-control" type="number" onChange={this.handleChangesForm} value={this.state.product.precio || '0'} id="textPrecio" />
				inputCantidad = <input ref={(campoCantidad)=>{this.cantidad=campoCantidad}} className="form-control" type="number" onChange={this.handleChangesForm} value={this.state.product.cantidad || '0'} id="textCantidad" />
				boton = <button onClick={this.productSubmit} className="btn btn-primary center-block" >Modificar</button>
				botonVolver = <button onClick={this.paginaProducto} className="btn btn-primary center-block" >Volver</button>
			}
			
		}
		if(this.props.product == 'component') {
			if(this.props.estado == 'nuevo') {
				titulo = <h2 className="text-center">Nuevo producto</h2>
				opcionesTipo = <select ref={(campoTipo)=>{this.tipo=campoTipo}} className="form-control" id="selectTipo"><option>Manillar</option><option>Potencia</option></select>
				opcionesMarca = <select ref={(campoMarca)=>{this.marca=campoMarca}} className="form-control" id="selectMarca"><option>RockShox</option><option>Massi</option><option>Cannondale</option><option>ZIPP</option></select>
				inputPrecio = <input ref={(campoPrecio)=>{this.precio=campoPrecio}} className="form-control" type="number" defaultValue="0" id="textPrecio" />
				inputCantidad = <input ref={(campoCantidad)=>{this.cantidad=campoCantidad}} className="form-control" type="number" defaultValue="0" id="textCantidad" />
				boton = <button onClick={this.productSubmit} className="btn btn-primary center-block" >Añadir</button>
				botonVolver = <button onClick={this.paginaProducto} className="btn btn-primary center-block" >Volver</button>
			}
			if(this.props.estado == 'detalles') {
				titulo = <h2 className="text-center">Detalles del producto con id: {this.props.id}</h2>
				opcionesTipo = <p>{this.state.product.tipo}</p>
				opcionesMarca = <p>{this.state.product.marca}</p>
				inputPrecio = <p>{this.state.product.precio}</p>
				inputCantidad = <p>{this.state.product.cantidad}</p>
				botonVolver = <button onClick={this.paginaProducto} className="btn btn-primary center-block" >Volver</button>
			} 
			if(this.props.estado == 'modificar') {
				titulo = <h2 className="text-center">Modificar el producto con id: {this.props.id}</h2>
				opcionesTipo = <select ref={(campoTipo)=>{this.tipo=campoTipo}} className="form-control" id="selectTipo" onChange={this.handleChangesForm} value={this.state.product.tipo}><option>Manillar</option><option>Potencia</option></select>
				opcionesMarca = <select ref={(campoMarca)=>{this.marca=campoMarca}} className="form-control" id="selectMarca" onChange={this.handleChangesForm} value={this.state.product.marca}><option>RockShox</option><option>Massi</option><option>Cannondale</option><option>ZIPP</option></select>
				inputPrecio = <input ref={(campoPrecio)=>{this.precio=campoPrecio}} className="form-control" type="number" onChange={this.handleChangesForm} value={this.state.product.precio || '0'} id="textPrecio" />
				inputCantidad = <input ref={(campoCantidad)=>{this.cantidad=campoCantidad}} className="form-control" type="number" onChange={this.handleChangesForm} value={this.state.product.cantidad || '0'} id="textCantidad" />
				boton = <button onClick={this.productSubmit} className="btn btn-primary center-block" >Modificar</button>
				botonVolver = <button onClick={this.paginaProducto} className="btn btn-primary center-block" >Volver</button>
			}
		}
		if(this.props.product == 'accesory') {
			if(this.props.estado == 'nuevo') {
				titulo = <h2 className="text-center">Nuevo producto</h2>
				opcionesTipo = <select ref={(campoTipo)=>{this.tipo=campoTipo}} className="form-control" id="selectTipo"><option>Bidon</option><option>Bomba</option></select>
				opcionesMarca = <select ref={(campoMarca)=>{this.marca=campoMarca}} className="form-control" id="selectMarca"><option>RockShox</option><option>Massi</option><option>Cannondale</option><option>ZIPP</option></select>
				inputPrecio = <input ref={(campoPrecio)=>{this.precio=campoPrecio}} className="form-control" type="number" defaultValue="0" id="textPrecio" />
				inputCantidad = <input ref={(campoCantidad)=>{this.cantidad=campoCantidad}} className="form-control" type="number" defaultValue="0" id="textCantidad" />
				boton = <button onClick={this.productSubmit} className="btn btn-primary center-block" >Añadir</button>
				botonVolver = <button onClick={this.paginaProducto} className="btn btn-primary center-block" >Volver</button>
			}
			if(this.props.estado == 'detalles') {
				console.log('Estoi en detalles')
				titulo = <h2 className="text-center">Detalles del producto con id: {this.props.id}</h2>
				opcionesTipo = <p>{this.state.product.tipo}</p>
				opcionesMarca = <p>{this.state.product.marca}</p>
				inputPrecio = <p>{this.state.product.precio}</p>
				inputCantidad = <p>{this.state.product.cantidad}</p>
				botonVolver = <button onClick={this.paginaProducto} className="btn btn-primary center-block" >Volver</button>
			} 
			if(this.props.estado == 'modificar') {
				titulo = <h2 className="text-center">Modificar el producto con id: {this.props.id}</h2>
				opcionesTipo = <select ref={(campoTipo)=>{this.tipo=campoTipo}} className="form-control" id="selectTipo" onChange={this.handleChangesForm} value={this.state.product.tipo}><option>Bidon</option><option>Bomba</option></select>
				opcionesMarca = <select ref={(campoMarca)=>{this.marca=campoMarca}} className="form-control" id="selectMarca" onChange={this.handleChangesForm} value={this.state.product.marca}><option>RockShox</option><option>Massi</option><option>Cannondale</option><option>ZIPP</option></select>
				inputPrecio = <input ref={(campoPrecio)=>{this.precio=campoPrecio}} className="form-control" type="number" onChange={this.handleChangesForm} value={this.state.product.precio || '0'} id="textPrecio" />
				inputCantidad = <input ref={(campoCantidad)=>{this.cantidad=campoCantidad}} className="form-control" type="number" onChange={this.handleChangesForm} value={this.state.product.cantidad || '0'} id="textCantidad" />
				boton = <button onClick={this.productSubmit} className="btn btn-primary center-block" >Modificar</button>
				botonVolver = <button onClick={this.paginaProducto} className="btn btn-primary center-block" >Volver</button>
			}
		}
		return (
			<div className="container">
				{titulo}
				<div className="row">
					<div className="col-sm-5"></div>
					<div className="col-sm-3">
						<div className="form-group">
						    <label htmlFor="selectTipo">Tipo</label>
						    {opcionesTipo}
						    <label htmlFor="selectMarca">Marca</label>
						    {opcionesMarca}
					    </div>
					    <div className="form-group">
					    	<label htmlFor="textPrecio">Precio</label>
							{inputPrecio}
						</div>
						<div className="form-group">
					    	<label htmlFor="textCantidad">Cantidad</label>
							{inputCantidad}
						</div>
						<div className="form-group row">
							{boton}<br />{botonVolver}
						</div>
					</div>
					<div className="col-sm-5"></div>
				</div>
			</div>
		);
	}
});

export default Product;