import React from 'react';

const Pagination = React.createClass({
	paginationFirst: function() { this.props.handlePagination(this.props.paginationLinks.first.href) },
	paginationPrev: function() { this.props.handlePagination(this.props.paginationLinks.prev.href) },
	paginationNext: function() { this.props.handlePagination(this.props.paginationLinks.next.href) },
	paginationLast: function() { this.props.handlePagination(this.props.paginationLinks.last.href) },
	render: function() {
		const styleCursorMenu = {
			cursor: 'default'
		};
		var links = []
		if (this.props.paginationLinks.self) {
			if(this.props.paginationLinks.first) links.push(<li style={styleCursorMenu} key={1}><a onClick={this.paginationFirst}>Primero</a></li>)
			else links.push(<li style={styleCursorMenu} className="disabled" key={1}><a>Primero</a></li>)
			if(this.props.paginationLinks.prev)	links.push(<li style={styleCursorMenu} key={2}><a onClick={this.paginationPrev}>Anterior</a></li>)
			else links.push(<li style={styleCursorMenu} className="disabled" key={2}><a>Anterior</a></li>)
			if(this.props.paginationLinks.next) links.push(<li style={styleCursorMenu} key={3}><a onClick={this.paginationNext}>Siguiente</a></li>)
			else links.push(<li style={styleCursorMenu} className="disabled" key={3}><a>Siguiente</a></li>)
			if(this.props.paginationLinks.last) links.push(<li style={styleCursorMenu} key={4}><a onClick={this.paginationLast}>Ultimo</a></li>)
			else links.push(<li style={styleCursorMenu} className="disabled" key={4}><a>Ultimo</a></li>)
		}	
		return (
			<div className="container text-center">
				<ul className="pagination">
				  {links}
				</ul>
			</div>
		);
	}
});

export default Pagination;