import React from 'react';
import Auth from './services/auth';

const Home = React.createClass({
	render: function() {
		var mensaje = <h2>Bienvenido!</h2>
		if(Auth.isAuthenticated()) mensaje = <h2>Bienvenido! {Auth.getLogin()}</h2>
		return (
			<div className="container">
				{mensaje}
			</div>
		);
	}
});

export default Home;