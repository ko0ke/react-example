import React from 'react';
import Listado from './listado'
import Pagination from './pagination'

const Bikes = React.createClass({
	getInitialState: function() {
		return {items: [], pagination: [], url: 'http://localhost:3000/bikes'}
	},
	refrescarItems: function(itemList, linksList, urlPagination) {
		this.setState({items: itemList, pagination: linksList, url: urlPagination})
	},
	componentWillMount: function() {
		fetch(this.state.url, {
			method: 'GET',
			headers: {
				'Content-type':'application/json'
			}
		}).then((response) => { 
			if (response.ok) return response.json()
		}).then((responseData) => {
			if (responseData) {
				if (typeof responseData._embedded !== 'undefined') this.refrescarItems(responseData._embedded.bikes, responseData._links, this.state.url)
				else this.refrescarItems('', responseData._links, this.state.url)
			}
		})
	},
	forceRefresh: function() {
		this.changePage('localhost:3000/bikes')
	},
	changePage: function(urlPagination) {
		fetch('http://'+urlPagination, {
			method: 'GET',
			headers: {
				'Content-type':'application/json'
			}
		}).then((response) => { 
			if (response.ok) return response.json()
		}).then((responseData) => {
			if (responseData) {
				if (typeof responseData._embedded !== 'undefined') this.refrescarItems(responseData._embedded.bikes, responseData._links, urlPagination)
				else this.refrescarItems('', responseData._links, urlPagination)
			}
		})
	},
	cambiarComponente: function(estado, login, id) {
		this.props.handleCambiarComponente(estado, login, id)
	},
	cambiarComponenteNuevo: function() {
		this.props.handleCambiarComponente('new-bike', this.props.login, '')
	},
	render: function() {
		return (
			<div className="container">
				<h2>Listado de Bicicletas</h2>
				<br />
				<Listado handleForceRefresh={this.forceRefresh} handleCambiarComponente={this.cambiarComponente} login={this.props.login} productos={this.state.items} product={this.props.product}/>
				<button type="button" className="btn btn-primary" onClick={this.cambiarComponenteNuevo}>Añadir Bicicleta</button>
				<br />
				<Pagination handlePagination={this.changePage} paginationLinks={this.state.pagination}/>
			</div>
		);
	}
});

export default Bikes;