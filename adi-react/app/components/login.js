import React from 'react';
import Auth from './services/auth'

const Login = React.createClass({
	getInitialState: function() {
		return {error: ''}
	},
	cambiarComponente: function() {
		this.props.handleCambiarComponente('home', true, '')
	},
	loginSubmit: function(event) {
		var usuario = {
			login: this.login.value,
			password: this.password.value
		}
		fetch('http://localhost:3000/users/login', {
			method: 'POST',
			headers: {
				'Content-type':'application/json'
			},
			body: JSON.stringify(usuario)
		}).then((response) => { 
			if (response.ok) return response.json()
			else this.setState({error: 'Usuario incorrecto'})
		}).then((responseData) => {
			if (responseData) {
				Auth.authentication(responseData.token, usuario.login)
				this.cambiarComponente()
			}
		})
		event.preventDefault()
	},
	handleOnChange: function() {
		this.setState({error: ''})
	},
	render: function() {
		return (
			<div className="container">
				<div className="row">
					<div className="col-sm-5"></div>
					<div className="col-sm-2">
						<h2>Login</h2>
						<form onSubmit={this.loginSubmit} method= "POST">
							<div className="form-group">
								<label htmlFor="lblLogin">Login</label>
								<input type="text" onChange={this.handleOnChange} ref={(campoLogin)=>{this.login=campoLogin}} name="txtLogin" className="form-control" placeholder="Login" required />
							</div>
							<div className="form-group">
								<label htmlFor="lblLogin">Password</label>
								<input type="password" onChange={this.handleOnChange} ref={(campoPassword)=>{this.password=campoPassword}}  name="txtPassword" className="form-control" placeholder="Password" required />
							</div>
							<button className="btn btn-primary center-block" >Entrar</button>
							<br />
							<p className="bg-danger text-center">{this.state.error}</p>
						</form>
					</div>
					<div className="col-sm-5"></div>
				</div>
			</div>
		);
	}
});

export default Login;