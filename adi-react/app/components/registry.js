import React from 'react';

const Registry = React.createClass({
	getInitialState: function() {
		return {error: ''}
	},
	cambiarComponente: function() {
		this.props.handleCambiarComponente('login', false, '')
	},
	handleOnChange: function() {
		this.setState({error: ''})
	},
	loginSubmit: function(event) {
		if(this.password.value != this.repetirpassword.value) {
			this.setState({error: 'Los password deben coincidir'})
		} else {
			var usuario = {
				login: this.login.value,
				password: this.password.value,
				email: this.email.value,
				admin: true
			}
			fetch('http://localhost:3000/users', {
				method: 'POST',
				headers: {
					'Content-type':'application/json'
				},
				body: JSON.stringify(usuario)
			}).then((response) => { 
				if (response.ok) return response.json()
			}).then((responseData) => {
				if (responseData) {
					this.cambiarComponente()
				}
			})
		}
		event.preventDefault()
	},
	render: function() {
		return (
			<div className="container">
				<div className="row">
					<div className="col-sm-4"></div>
					<div className="col-sm-4">
						<h2>Registro de usuario</h2>
						<form onSubmit={this.loginSubmit} method= "POST">
							<div className="form-group">
								<label htmlFor="lblLogin">Login</label>
								<input type="text" onChange={this.handleOnChange} ref={(campoLogin)=>{this.login=campoLogin}} name="txtLogin" className="form-control" placeholder="Login" required />
							</div>
							<div className="form-group">
								<label htmlFor="lblEmail">Email</label>
								<input type="text" onChange={this.handleOnChange} ref={(campoEmail)=>{this.email=campoEmail}} name="txtEmail" className="form-control" placeholder="Email" required />
							</div>
							<div className="form-group">
								<label htmlFor="lblPassword">Password</label>
								<input type="password" onChange={this.handleOnChange} ref={(campoPassword)=>{this.password=campoPassword}}  name="txtPassword" className="form-control" placeholder="Password" required />
							</div>
							<div className="form-group">
								<label htmlFor="lblRepetirPassword">Repetir password</label>
								<input type="password" onChange={this.handleOnChange} ref={(campoRepetirPassword)=>{this.repetirpassword=campoRepetirPassword}}  name="txtRepetirPassword" className="form-control" placeholder="Repite el password" required />
							</div>
							<button className="btn btn-primary center-block" >Entrar</button>
							<br />
							<p className="bg-danger text-center">{this.state.error}</p>
						</form>
					</div>
					<div className="col-sm-4"></div>
				</div>
			</div>
		);
	}
});

export default Registry;