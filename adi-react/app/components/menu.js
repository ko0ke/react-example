import React from 'react';
import Auth from './services/auth';

const Menu = React.createClass({
	paginaHome: function() { this.props.handleCambiarComponente('home', this.props.login, '') },
	paginaBicicletas: function() { this.props.handleCambiarComponente('bikes', this.props.login, '')	},
	paginaAccesorios: function() { this.props.handleCambiarComponente('accesories', this.props.login, '')	},
	paginaComponentes: function() { this.props.handleCambiarComponente('components', this.props.login, '')	},
	paginaLogin: function() { this.props.handleCambiarComponente('login', this.props.login, '') },
	paginaRegistro: function() { this.props.handleCambiarComponente('registry', this.props.login, '')	},
	paginaLogout: function() { 
		Auth.logout()
		this.props.handleCambiarComponente('login', false, '') 
	},
	render: function() {
		const styleCursorMenu = {
			cursor: 'default'
		};
		var menuUsuario;
		if(this.props.login == false) menuUsuario = <ul className="nav navbar-nav navbar-right"><li style={styleCursorMenu}><a onClick={this.paginaLogin}>Login</a></li><li style={styleCursorMenu}><a onClick={this.paginaRegistro}>Registro</a></li></ul>
		else menuUsuario = <ul className="nav navbar-nav navbar-right"><li style={styleCursorMenu}><a>{Auth.getLogin()}</a></li><li style={styleCursorMenu}><a onClick={this.paginaLogout}>Logout</a></li></ul>
		return (
			<nav className="navbar navbar-inverse">
				<div className="container-fluid">
					<ul className="nav navbar-nav">
						<li style={styleCursorMenu}><a onClick={this.paginaHome}>Home</a></li>
						<li style={styleCursorMenu}><a onClick={this.paginaBicicletas}>Bicicletas</a></li>
						<li style={styleCursorMenu}><a onClick={this.paginaAccesorios}>Accesorios</a></li>
						<li style={styleCursorMenu}><a onClick={this.paginaComponentes}>Componentes</a></li>
					</ul>
					{menuUsuario}
				</div>
			</nav>
		);
	}
});

export default Menu;