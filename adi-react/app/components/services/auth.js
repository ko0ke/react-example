class Auth {

	static authentication(token, loginUser) {
		localStorage.setItem('token', token);
		localStorage.setItem('login', loginUser);
	}

	static isAuthenticated() {
		if(localStorage.getItem('token')) return true;
		else return false;
	}

	static getToken() {
		return localStorage.getItem('token');
	}

	static getLogin() {
		return localStorage.getItem('login');
	}

	static logout() {
		localStorage.removeItem('token');
		localStorage.removeItem('login');
	}
}

export default Auth;