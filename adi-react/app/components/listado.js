import React from 'react';
import Auth from './services/auth';

const Listado = React.createClass({
	cambiarComponenteDetalles: function(event) {
		if(this.props.product == 'bike') this.props.handleCambiarComponente('details-bike', this.props.login, event.target.id)
		if(this.props.product == 'component') this.props.handleCambiarComponente('details-component', this.props.login, event.target.id)
		if(this.props.product == 'accesory') this.props.handleCambiarComponente('details-accesory', this.props.login, event.target.id)
	},
	cambiarComponenteModificar: function(event) {
		if(this.props.product == 'bike') this.props.handleCambiarComponente('update-bike', this.props.login, event.target.id)
		if(this.props.product == 'component') this.props.handleCambiarComponente('update-component', this.props.login, event.target.id)
		if(this.props.product == 'accesory') this.props.handleCambiarComponente('update-accesory', this.props.login, event.target.id)
	},
	removeProduct: function(event) {
		var url
		if(this.props.product == 'bike') url = 'http://localhost:3000/bikes/'
		if(this.props.product == 'component') url = 'http://localhost:3000/components/'
		if(this.props.product == 'accesory') url = 'http://localhost:3000/accesories/'
		if(!Auth.isAuthenticated()) this.props.handleCambiarComponente('login', this.props.login, '')
		else {
			var id = event.target.id
			fetch(url+id, {
				method: 'DELETE',
				headers: {
					'Authorization': 'Bearer ' + Auth.getToken(),
					'Content-type':'application/json'
				}
			}).then((response) => { 
				if (response.ok) this.props.handleForceRefresh()
			})
		}
		event.preventDefault()
	},
	render: function() {
		const styleCursorMenu = {
			cursor: 'default'
		};
		var filas = []
		if (typeof this.props.productos !== 'undefined') {
			if(typeof this.props.productos.length === 'undefined')
				filas.push(<tr key={0}><td><img src={this.props.productos.imagen} className="img-responsive" height="65" width="90" /></td><td>{this.props.productos.tipo}</td><td>{this.props.productos.marca}</td><td>{this.props.productos.precio}</td><td>{this.props.productos.cantidad}</td><td><button id={this.props.productos.id} type="button" onClick={this.cambiarComponenteDetalles} className="btn btn-info">Detalles</button>&nbsp;<button id={this.props.productos.id} type="button" onClick={this.cambiarComponenteModificar} className="btn btn-warning">Editar</button>&nbsp;<button id={this.props.productos.id} type="button" onClick={this.removeProduct} className="btn btn-danger">Eliminar</button></td></tr>)
        	for (var i=0; i<this.props.productos.length; i++) {
				filas.push(<tr key={i}><td><img src={this.props.productos[i].imagen} className="img-responsive" height="65" width="90" /></td><td>{this.props.productos[i].tipo}</td><td>{this.props.productos[i].marca}</td><td>{this.props.productos[i].precio}</td><td>{this.props.productos[i].cantidad}</td><td><button id={this.props.productos[i].id} type="button" onClick={this.cambiarComponenteDetalles} className="btn btn-info">Detalles</button>&nbsp;<button id={this.props.productos[i].id} type="button" onClick={this.cambiarComponenteModificar} className="btn btn-warning">Editar</button>&nbsp;<button id={this.props.productos[i].id} type="button" onClick={this.removeProduct} className="btn btn-danger">Eliminar</button></td></tr>)
			}
		}
		return (
			<div>
				<table className="table table-condensed">
					<thead>
						<tr>
							<th>Imagen</th>
							<th>Tipo</th>
							<th>Marca</th>
							<th>Precio</th>
							<th>Cantidad</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
				  		{filas}
				  	</tbody>
				</table>
			</div>
		);
	}
});

export default Listado;