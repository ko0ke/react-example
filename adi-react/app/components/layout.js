import React from 'react';
import Menu from './menu';
import Home from './home';
import Bikes from './bikes';
import Accesories from './accesories';
import Components from './components';
import Product from './product';
import Login from './login';
import Registry from './registry';
import Auth from './services/auth';

const Layout = React.createClass({
	getInitialState: function() {
		return {pagina: '', login: Auth.isAuthenticated(), id: ''}
	},
	cambiarComponente: function(pag, log, idItem) {
		this.setState({pagina: pag, login: log, id: idItem})
	},
	render: function() {
		var componente = <Login handleCambiarComponente={this.cambiarComponente}/>
		if(this.state.pagina == 'home') 										componente = <Home />
		
		if(this.state.pagina == 'bikes') 										componente = <Bikes handleCambiarComponente={this.cambiarComponente} login={this.state.login} product="bike"/>
		if(Auth.isAuthenticated() && this.state.pagina == 'new-bike')			componente = <Product estado="nuevo" product="bike" handleCambiarComponente={this.cambiarComponente} login={this.state.login}/>
		else if(!Auth.isAuthenticated() && this.state.pagina == 'new-bike')		componente = <Login handleCambiarComponente={this.cambiarComponente} />
		if(this.state.pagina == 'details-bike')									componente = <Product estado="detalles" product="bike" id={this.state.id} handleCambiarComponente={this.cambiarComponente} login={this.state.login}/>
		if(Auth.isAuthenticated() && this.state.pagina == 'update-bike')		componente = <Product estado="modificar" product="bike" id={this.state.id} handleCambiarComponente={this.cambiarComponente} login={this.state.login}/>
		else if(!Auth.isAuthenticated() && this.state.pagina == 'update-bike')	componente = <Login handleCambiarComponente={this.cambiarComponente} />

		if(this.state.pagina == 'accesories') 										componente = <Accesories handleCambiarComponente={this.cambiarComponente} login={this.state.login} product="accesory"/>
		if(Auth.isAuthenticated() && this.state.pagina == 'new-accesory')			componente = <Product estado="nuevo" product="accesory" handleCambiarComponente={this.cambiarComponente} login={this.state.login}/>
		else if(!Auth.isAuthenticated() && this.state.pagina == 'new-accesory')		componente = <Login handleCambiarComponente={this.cambiarComponente} />
		if(this.state.pagina == 'details-accesory')									componente = <Product estado="detalles" product="accesory" id={this.state.id} handleCambiarComponente={this.cambiarComponente} login={this.state.login}/>
		if(Auth.isAuthenticated() && this.state.pagina == 'update-accesory')		componente = <Product estado="modificar" product="accesory" id={this.state.id} handleCambiarComponente={this.cambiarComponente} login={this.state.login}/>
		else if(!Auth.isAuthenticated() && this.state.pagina == 'update-accesory')	componente = <Login handleCambiarComponente={this.cambiarComponente} />
		
		if(this.state.pagina == 'components') 										componente = <Components handleCambiarComponente={this.cambiarComponente} login={this.state.login} product="component"/>
		if(Auth.isAuthenticated() && this.state.pagina == 'new-component')			componente = <Product estado="nuevo" product="component" handleCambiarComponente={this.cambiarComponente} login={this.state.login}/>
		else if(!Auth.isAuthenticated() && this.state.pagina == 'new-component')	componente = <Login handleCambiarComponente={this.cambiarComponente} />
		if(this.state.pagina == 'details-component')								componente = <Product estado="detalles" product="component" id={this.state.id} handleCambiarComponente={this.cambiarComponente} login={this.state.login}/>
		if(Auth.isAuthenticated() && this.state.pagina == 'update-component')		componente = <Product estado="modificar" product="component" id={this.state.id} handleCambiarComponente={this.cambiarComponente} login={this.state.login}/>
		else if(!Auth.isAuthenticated() && this.state.pagina == 'update-component')	componente = <Login handleCambiarComponente={this.cambiarComponente} />

		if(this.state.pagina == 'login') 										componente = <Login handleCambiarComponente={this.cambiarComponente} />
		
		if(this.state.pagina == 'registry') 									componente = <Registry handleCambiarComponente={this.cambiarComponente}/>
		
		return (
			<div className="container">
				<Menu handleCambiarComponente={this.cambiarComponente} login={this.state.login}/>
				{componente}
			</div>
		);
	}
});

export default Layout