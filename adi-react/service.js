var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('./config');

exports.crearToken = function(item) {
  var payload = {
    sub: item._id,
    iat: moment().unix(),
    exp: moment().add(86400, "seconds").unix(),
    admin: item.admin
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
};