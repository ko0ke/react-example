var express = require('express');
var router = express.Router();
var User = require('../models/user');
var Halson = require('halson');
var auth = require('../middlewares/auth');
var service = require('../service');

// GET ALL
router.get('/', function(req, res){
  var page = (req.query.page === undefined) ? 1 : req.query.page;
  var itemsPerPage = 99999;
  var totalPages = -1;
  User.count(function(err, count){
    if(err) res.status(500).send({message: "Internal Server Error."});
    else  totalPages = Math.round(count / itemsPerPage);
  });
  User.find().skip((page - 1)*itemsPerPage).limit(itemsPerPage).exec(function(err, list) {
    if(err) res.status(500).send({message: "Internal Server Error."});
    else {
      // PAGINACION
      var resource = new Halson({items : itemsPerPage});
      res.setHeader('Location',req.headers.host + '' + req.originalUrl);
      resource.addLink('self',req.headers.host + '' + req.originalUrl);
      if(page > 2) resource.addLink('first',req.headers.host + '' + req.baseUrl);
      if(page == 2) resource.addLink('prev',req.headers.host + '' + req.baseUrl);
      else if(page > 2) resource.addLink('prev',req.headers.host + '' + req.baseUrl + '?page=' + (page - 1));
      if(page < totalPages) resource.addLink('next',req.headers.host + '' + req.baseUrl + '?page=' + (parseInt(page) + 1));
      if(page < (totalPages-1)) resource.addLink('last',req.headers.host + '' + req.baseUrl + '?page=' + totalPages);
      // HIPERMEDIA
      for(var item of list)
      {
        var embed = {
          id: item._id,
          login: item.login,
          email: item.email,
          _links: [
            {rel: 'self', href: req.headers.host + '' + req.baseUrl + '/' + item._id},
            {rel: 'edit', href: req.headers.host + '' + req.baseUrl + '/' + item._id},
            {rel: 'delete', href: req.headers.host + '' + req.baseUrl + '/' + item._id}
          ]
        };
        resource.addEmbed('users', embed);
      }
      console.log('[CONTROLLERS] [users.js] [function getAll] Lista de usuarios devuelta!');
      res.status(200).send(resource);
    }
  })
});

// GET ONE
router.get('/:id', function (req, res) {
  User.findById(req.params.id,function(err, item) {
    if(err || !item)  res.status(404).send({message: "No existe el recurso."});
    else {
      // HIPERMEDIA
      var resource = new Halson({});
      var embed = {
        id: item._id,
        login: item.login,
        email: item.email,
        password: item.password,
        admin: item.admin,
        _links: [
          {rel: 'self', href: req.headers.host + '' + req.baseUrl + '/' + item._id},
          {rel: 'edit', href: req.headers.host + '' + req.baseUrl + '/' + item._id},
          {rel: 'delete', href: req.headers.host + '' + req.baseUrl + '/' + item._id}
        ]
      };
      console.log('[CONTROLLERS] [users.js] [function getOne] Usuario devuelto!');
      res.setHeader('Location',req.headers.host + '' + req.baseUrl + '/' + item._id);
      resource.addEmbed('item', embed);
      res.status(200).send(embed);
    }
  });
});

// CREATE
router.post('/', function(req, res) {
  var nuevo = req.body;
  if (nuevo.login && nuevo.password && nuevo.email) {
    var nUser = User({
      login: nuevo.login,
      email: nuevo.email,
      password: nuevo.password,
      admin: nuevo.admin
    });
    nUser.save(function (err) {
      if(err) res.status(500).send({message: "Internal Server Error."});
      else {
        console.log('[CONTROLLERS] [users.js] [function create] Usuario creado!');
        res.setHeader('Location',req.headers.host + '' + req.baseUrl + '/' + nUser._id);
        res.status(201).send(nUser);
      }
    })
  }
  else res.status(400).send({message: "La solicitud contiene sintaxis errónea."});
});

// UPDATE
router.put('/:id', function(req, res) {
  if (req.body.login && req.body.password && req.body.email) {
    User.findById(req.params.id,function(err, item){
      if(err || !item)  res.status(404).send({message: "No existe el recurso."});
      else {
        item.login= req.body.login;
        item.email= req.body.email;
        item.password= req.body.password;
        item.save(function (err) {
          if(err) res.status(500).send({message: "Internal Server Error."});
          else {
            console.log('[CONTROLLERS] [users.js] [function update] Usuario modificado!');
            res.setHeader('Location',req.headers.host + '' + req.baseUrl + '/' + item._id);
            res.status(201).send(item);
          }
        });
      }
    });
  }
  else  res.status(400).send({message: "La solicitud contiene sintaxis errónea."});
});

// DELETE
router.delete('/:id', function(req, res) {
  User.findById(req.params.id,function(err, item){
    if(err || !item)  res.status(404).send({message: "No existe el recurso."});
    else {
      item.remove(function(err){
        if(err) res.status(500).send({message: "Internal Server Error."});
        else {
          console.log('[CONTROLLERS] [users.js] [function remove] Usuario eliminado!');
          res.status(204).end();
        }
      });
    }
  })
});

// LOGIN
router.post('/login', function(req, res) {
  if (req.body.login && req.body.password) {
    //console.log('[CONTROLLERS] [users.js] [function login] => user [' + req.body.login + '][' + req.body.password + ']');
    User.findOne({login: req.body.login, password: req.body.password}, function(err, item) {
      if(!err && item != null) {
        console.log('[CONTROLLERS] [users.js] [function login] Usuario correcto!');
        res.status(200).send({token: service.crearToken(item)});
      }
      else res.status(403).send({message: "Datos de autenticacion invalidos"});
    });
  }
  else res.status(400).send({message: "La solicitud contiene sintaxis errónea."});
})

module.exports = router;