var express = require('express');
var router = express.Router();
var Accesory = require('../models/accesory');
var Halson = require('halson');
var auth = require('../middlewares/auth');
var service = require('../service');

// GET ALL
router.get('/', function(req, res){
  var page = (req.query.page === undefined) ? 1 : req.query.page;
  var itemsPerPage = 3;
  var totalPages = -1;
  Accesory.count(function(err, count){
    if(err) res.status(500).send({message: "Internal Server Error."});
    else  totalPages = Math.round(count / itemsPerPage);
  });
  Accesory.find().skip((page - 1)*itemsPerPage).limit(itemsPerPage).exec(function(err, list) {
    if(err) res.status(500).send({message: "Internal Server Error."});
    else {
      // PAGINACION
      var resource = new Halson({items : itemsPerPage});
      res.setHeader('Location',req.headers.host + '' + req.originalUrl);
      resource.addLink('self',req.headers.host + '' + req.originalUrl);
      if(page > 2) resource.addLink('first',req.headers.host + '' + req.baseUrl);
      if(page == 2) resource.addLink('prev',req.headers.host + '' + req.baseUrl);
      else if(page > 2) resource.addLink('prev',req.headers.host + '' + req.baseUrl + '?page=' + (page - 1));
      if(page < totalPages) resource.addLink('next',req.headers.host + '' + req.baseUrl + '?page=' + (parseInt(page) + 1));
      if(page < (totalPages-1)) resource.addLink('last',req.headers.host + '' + req.baseUrl + '?page=' + totalPages);
      // HIPERMEDIA
      for(var item of list)
      {
        var embed = {
          id: item._id,
          tipo: item.tipo,
          marca: item.marca,
          precio: item.precio,
          cantidad: item.cantidad,
          imagen: item.imagen,
          _links: [
            {rel: 'self', href: req.headers.host + '' + req.baseUrl + '/' + item._id},
            {rel: 'edit', href: req.headers.host + '' + req.baseUrl + '/' + item._id},
            {rel: 'delete', href: req.headers.host + '' + req.baseUrl + '/' + item._id}
          ]
        };
        resource.addEmbed('accesories', embed);
      }
      console.log('[CONTROLLERS] [accesories.js] [function getAll] Lista de accesorios devuelta!');
      res.status(200).send(resource);
    }
  })
});

// GET ONE
router.get('/:id', function (req, res) {
  Accesory.findById(req.params.id,function(err, item) {
    if(err || !item)  res.status(404).send({message: "No existe el recurso."});
    else {
      // HIPERMEDIA
      var resource = new Halson({});
      var embed = {
        id: item._id,
        tipo: item.tipo,
        marca: item.marca,
        precio: item.precio,
        cantidad: item.cantidad,
        imagen: item.imagen,
        _links: [
          {rel: 'self', href: req.headers.host + '' + req.baseUrl + '/' + item._id},
          {rel: 'edit', href: req.headers.host + '' + req.baseUrl + '/' + item._id},
          {rel: 'delete', href: req.headers.host + '' + req.baseUrl + '/' + item._id}
        ]
      };
      console.log('[CONTROLLERS] [accesories.js] [function getOne] Accesorio devuelto!');
      res.setHeader('Location',req.headers.host + '' + req.baseUrl + '/' + item._id);
      resource.addEmbed('item', embed);
      res.status(200).send(embed);
    }
  });
});

// CREATE
router.post('/', auth.checkAuth, function(req, res) {
  var nuevo = req.body;
  if (nuevo.tipo && nuevo.marca) {
    var nItem = Accesory({
      tipo: nuevo.tipo,
      marca: nuevo.marca,
      precio: nuevo.precio,
      cantidad: nuevo.cantidad,
      imagen: 'images/default.png',
    });
    nItem.save(function (err) {
      if(err) res.status(500).send({message: "Internal Server Error."});
      else {
        console.log('[CONTROLLERS] [accesories.js] [function create] Accesorio creado!');
        res.setHeader('Location',req.headers.host + '' + req.baseUrl + '/' + nItem._id);
        res.status(201).send(nItem);
      }
    })
  }
  else res.status(400).send({message: "La solicitud contiene sintaxis errónea."});
});

// UPDATE
router.put('/:id', auth.checkAuth, function(req, res) {
  if (req.body.tipo && req.body.marca) {
    Accesory.findById(req.params.id,function(err, item){
      if(err || !item)  res.status(404).send({message: "No existe el recurso."});
      else {
        item.tipo= req.body.tipo;
        item.marca= req.body.marca;
        item.precio= req.body.precio;
        item.cantidad= req.body.cantidad;
        item.save(function (err) {
          if(err) res.status(500).send({message: "Internal Server Error."});
          else {
            console.log('[CONTROLLERS] [accesories.js] [function update] Accesorio modificado!');
            res.setHeader('Location',req.headers.host + '' + req.baseUrl + '/' + item._id);
            res.status(201).send(item);
          }
        });
      }
    });
  }
  else  res.status(400).send({message: "La solicitud contiene sintaxis errónea."});
});

// DELETE
router.delete('/:id', auth.checkAuth, function(req, res) {
  Accesory.findById(req.params.id,function(err, item){
    if(err || !item)  res.status(404).send({message: "No existe el recurso."});
    else {
      item.remove(function(err){
        if(err) res.status(500).send({message: "Internal Server Error."});
        else {
          console.log('[CONTROLLERS] [accesories.js] [function remove] Accesorio eliminado!');
          res.status(204).end();
        }
      });
    }
  })
});

module.exports = router;