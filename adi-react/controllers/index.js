var express = require('express');
var router = express.Router();

// USUARIOS
router.use('/users',require('./users'));
// PRODUCTOS
router.use('/bikes',require('./bikes'));
router.use('/components',require('./components'));
router.use('/accesories',require('./accesories'));

module.exports = router;