var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('../config');

exports.checkAuth = function(pet, resp, next) {
  if(!pet.headers.authorization) return resp.status(403).send({message: "No dispone de permisos suficientes."});
  var payload = -1;
  var token = -1;
  try {
    token = pet.headers.authorization.split(" ")[1];
  } catch (e) {}
  try {
    payload = jwt.decode(token, config.TOKEN_SECRET);
  } catch (e) {
    resp.setHeader('WWW-Authenticate','Bearer');
    return resp.status(401).send({message: "El token ha expirado"});
  }
  pet.user = payload;
  next();
}