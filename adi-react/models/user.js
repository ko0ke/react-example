var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  login: String,
  password: String,
  email: String,
  admin: {
    type: Boolean,
    default: false
  }
});

var User = mongoose.model('users', userSchema);

module.exports = User;