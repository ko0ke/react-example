var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var componentSchema = new Schema({
  tipo: String,
  marca: String,
  precio: String,
  cantidad: String,
  imagen: String
});

var Component = mongoose.model('components', componentSchema);

module.exports = Component;