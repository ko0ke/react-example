var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bikeSchema = new Schema({
  tipo: String,
  marca: String,
  precio: String,
  cantidad: String,
  imagen: String
});

var Bike = mongoose.model('bikes', bikeSchema);

module.exports = Bike;