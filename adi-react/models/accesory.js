var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var accesorySchema = new Schema({
  tipo: String,
  marca: String,
  precio: String,
  cantidad: String,
  imagen: String
});

var Accesory = mongoose.model('accesories', accesorySchema);

module.exports = Accesory;